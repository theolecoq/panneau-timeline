import L from 'leaflet'
import Timeline from './timeline'

const addElementsToHomePage = event => {
    let timelineContainer = document.querySelector('#timeline');
    let timelineElementTemplate = document.querySelector('#timeline-item-template');

    Object.entries(Timeline.instance).sort(entry => entry[0]).forEach(item => {
        let timelineElement = timelineElementTemplate.cloneNode(true);
        timelineElement.removeAttribute('id');
        timelineElement.classList.remove('hidden');
        timelineElement.addEventListener('click', event => {
            window.location = `details.html?item=${item[0]}`
        });

        timelineElement.querySelector('.timeline-item-title').textContent = new Date(+item[0]).toLocaleString();

        Object.keys(item[1]).forEach(key => {
            switch (key) {
                case 'text':
                    var contentElement = document.createElement('p');
                    contentElement.textContent = item[1]['text'];
                    break;
                case 'locale':
                    var contentElement = document.createElement('div');
                    contentElement.classList.add('map-container');
                    break;
                case 'photo':
                    var contentElement = document.createElement('img');
                    contentElement.setAttribute('src', item[1]['photo']);
                    contentElement.classList.add('w-full');
                    contentElement.classList.add('h-auto');
                    contentElement.style.objectFit = 'cover';
                    contentElement.style.objectPosition = 'center';
                    break;
                case 'video':
                    var contentElement = document.createElement('video');
                    contentElement.setAttribute('src', item[1]['video']);
                    contentElement.setAttribute('controls', '');
                    contentElement.classList.add('w-full');
                    contentElement.classList.add('h-auto');
                    break;
            }
            timelineElement.querySelector('.timeline-item-content').appendChild(contentElement);
        });
        timelineContainer.appendChild(timelineElement);
        if (item[1].hasOwnProperty('locale')) {
            let contentElement = timelineElement.querySelector('.map-container');
            contentElement.style.height = '8rem';
            let map = L.map(contentElement).setView(item[1]['locale'], 13);

            // disable interactions; map for viewing only
            map.dragging.disable();
            map.touchZoom.disable();
            map.doubleClickZoom.disable();
            map.scrollWheelZoom.disable();
            map.boxZoom.disable();
            map.keyboard.disable();
            if (map.tap) map.tap.disable();
            contentElement.style.cursor = 'default';
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            L.marker(item[1]['locale']).addTo(map);
        }
    });
}

if (document.location.href.endsWith('index.html')) {
    document.addEventListener('deviceready', addElementsToHomePage, false);
}