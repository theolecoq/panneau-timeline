import Timeline from './timeline';

const makeDetailsCard = event => {
    let detailsCard = document.getElementById('item-details');
    let detailsTitle = detailsCard.querySelector('.timeline-item-title');
    let detailsContainer = detailsCard.querySelector('.timeline-item-content');

    const urlParams = new URLSearchParams(window.location.search);
    const timelineItem = Timeline.get(urlParams.get('item'));

    detailsTitle.textContent = new Date(+urlParams.get('item')).toLocaleString();

    Object.keys(timelineItem).forEach(key => {
        switch (key) {
            case 'text':
                var contentElement = document.createElement('p');
                contentElement.textContent = timelineItem['text'];
                break;
            case 'locale':
                var contentElement = document.createElement('div');
                contentElement.classList.add('map-container');
                break;
            case 'photo':
                var contentElement = document.createElement('img');
                contentElement.setAttribute('src', timelineItem['photo']);
                contentElement.classList.add('w-full');
                contentElement.classList.add('h-auto');
                contentElement.style.objectFit = 'cover';
                contentElement.style.objectPosition = 'center';
                break;
            case 'video':
                var contentElement = document.createElement('video');
                contentElement.setAttribute('src', timelineItem['video']);
                contentElement.setAttribute('controls', '');
                contentElement.classList.add('w-full');
                contentElement.classList.add('h-auto');
                break;
        }
        detailsContainer.appendChild(contentElement);
    });
    if (timelineItem.hasOwnProperty('locale')) {
        let contentElement = detailsContainer.querySelector('.map-container');
        contentElement.style.height = '8rem';
        let map = L.map(contentElement).setView(timelineItem['locale'], 13);

        // disable interactions; map for viewing only
        map.dragging.disable();
        map.touchZoom.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.boxZoom.disable();
        map.keyboard.disable();
        if (map.tap) map.tap.disable();
        contentElement.style.cursor = 'default';
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        L.marker(timelineItem['locale']).addTo(map);
    }
}

if (document.location.href.split('?')[0].endsWith('details.html')) {
    document.addEventListener('deviceready', makeDetailsCard, false);
}