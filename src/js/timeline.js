var timeline = localStorage.getItem('timeline') ? JSON.parse(localStorage.getItem('timeline')) : {};

export default class Timeline {
    static get instance() {
        return timeline;
    }

    static set(_key, _value) {
        timeline[_key] = _value;
        localStorage.setItem('timeline', JSON.stringify(timeline));
    }

    static get(_key) {
        return timeline[_key];
    }

    static delete(_key) {
        delete timeline[_key];
        localStorage.setItem('timeline', JSON.stringify(timeline));
    }
}