import L from 'leaflet'
import Timeline from './timeline'
import $ from 'jquery'

var toAdd = {};
var lat, lon;

const initMap = (lat, lon) => {
    let mapContainer = document.getElementById('map');
    mapContainer.style.height = '8rem';
    var macarte = null;
    macarte = L.map(mapContainer).setView([lat, lon], 11);
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);
    L.marker([lat, lon]).addTo(macarte);
}

const readUploadedFile = async (inputFile) => {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new Error("Une erreur a eu lieu pendant le chargement du fichier."));
        };

        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result);
        };
        temporaryFileReader.readAsDataURL(inputFile);
    });
};

const addInteractionsToAddPage = event => {
    $('input[type=file]').change(event => {
        if (event.target.files.length > 0) {
            readUploadedFile(event.target.files[0]).then(fileContents => {
                toAdd[event.target.id] = fileContents;
            });
        }
    });

    $('#map').hide();
    $('#geolocbutton').click(event => {
        event.target.textContent = 'Chargement...';

        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
            timeout: 10000
        });

        function onSuccess(position) {
            $('#map').show();
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            initMap(lat, lon);
            event.target.textContent = 'Position enregistrée avec succes.';
        }

        function onError() {
            event.target.textContent = 'Une erreur a eu lieu. Veuillez réessayer.';
        }

    });

    document.getElementById('add-form').addEventListener('submit', handleFormSubmit);
    document.getElementById('add-form').onsubmit = handleFormSubmit;
    document.getElementById('save').addEventListener('click', handleFormSubmit);
}

const handleFormSubmit = event => {
    event.preventDefault();

    if ($('#text').val() !== null) {
        toAdd['text'] = $('#text').val()
    }

    if (lat && lon) {
        toAdd['locale'] = [lon, lat]
    }
    Timeline.set(Date.now(), toAdd);

    window.location = 'index.html';

}

if (document.location.href.endsWith('add.html')) {
    document.addEventListener('deviceready', addInteractionsToAddPage, false);
}